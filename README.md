# european_electricity

Python notebooks and scripts for the European Electricity project.
In the notebook main_kmeans_clustering, the dataset (consisting of AT and BE only at the moment) is reduced using k-means clustering and the cluster centres are used as the typical-day representatives. 
