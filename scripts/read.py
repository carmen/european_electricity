import pandas as pd

# read data file, allows for different indices
# for now there are two options: timestep (1-8760) or UTC
# can add local time option in the script if needed
def zonal_data(file_path, datetime_index=False):
    if datetime_index is False:
        df=pd.read_excel(file_path, index_col=0)
    elif datetime_index =='UTC':
        df=pd.read_excel(file_path)
        df[df.columns[0]]=pd.to_datetime(df[df.columns[0]])
        df=df.set_index(df.columns[0])
        
    # drop identically 0 columns
    del_cols=list()
    for col in df.columns:
        if (df[col] == 0).all():
            print(col + ' is identically zero.')
            del_cols.append(col)
    df=df.drop(del_cols, axis=1)    
    
    return df
